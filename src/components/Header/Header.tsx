import * as React from "react";

export interface Props {}

export interface Dispatch {}

export interface State {}

export class Header extends React.Component<Props & Dispatch, State> {
  render() {
    return (
      <header className="col">
        <div className="row">
          <div className="col">
            <h1>Профиль Агента</h1>
          </div>
          <div className="col">
            <div className="row">
              <h1>Владимир</h1>
            </div>
            <div className="row">
              <h5 className="badge badge-secondary">Владелец: Егор</h5>
            </div>
          </div>
        </div>
      </header>
    );
  }
}
