import * as React from "react";
import { connect } from "react-redux";

import * as css from "./styles.less";

export interface Props {}

export interface Dispatch {}

export interface State {}

class Component extends React.Component<Props & Dispatch, State> {
  render() {
    return (
      <article className={css.mainBody}>
        <button type="button" className="btn btn-primary">
          Primary
        </button>
      </article>
    );
  }
}

type MapStateToProps = Props;

const mapStateToProps = (): MapStateToProps => ({});

type MapDispatchToProps = Dispatch;

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({});

export default connect<MapStateToProps, MapDispatchToProps, {}>(
  mapStateToProps,
  mapDispatchToProps
)(Component);
