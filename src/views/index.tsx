import * as React from "react";
import { Switch, Route, Router } from "react-router-dom";
import history from "apphistory";

import Header from "components/Header";
import Landing from "views/Landing";

export default () => (
  <div className="container">
    <div className="row">
      <Header />
    </div>

    <Router history={history}>
      <Switch>
        <Route exact path="/" component={Landing} />
      </Switch>
    </Router>
  </div>
);
