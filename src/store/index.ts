import { combineReducers, createStore, applyMiddleware } from "redux";

import { createLogger } from "redux-logger";

const loggerMiddleware = createLogger();

const store = createStore(
  combineReducers({
    app: () => ({}),
    db: () => ({})
  }),
  applyMiddleware(loggerMiddleware)
);

export default store;
